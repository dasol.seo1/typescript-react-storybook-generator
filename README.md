This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# typescript-react-generator

## 적용한 것과 그 이유

-   React([https://ko.reactjs.org/](https://ko.reactjs.org/))
    -   프론트엔드 프레임워크 중 사용하는 비율이 압도적이다.
        [angular vs react vs vue | npm trends](https://www.npmtrends.com/angular-vs-react-vs-vue)
-   Typescript([https://www.typescriptlang.org/](https://www.typescriptlang.org/))
    -   Javascript은 약한 타입 언어이기 때문에 개발하면서 실수할 가능성이 크다.
    -   이러한 점을 보완하기 위해 타입스크립트를 적용했다.
-   SCSS([https://sass-lang.com/](https://sass-lang.com/))
    -   스타일 적용시 클래스 이름의 중복 이슈를 해결할 수 있다.
    -   프로그래밍 언어를 사용하듯이 스타일을 적용할 수 있다.
-   ESLint([https://eslint.org/](https://eslint.org/))
    -   Javascript 인터프리터 언어이기 때문에 런타임 에러가 발생할 가능성이 크다.
    -   이러한 점을 보완하기 위해 사전에 에러를 잡아주는 ESLint를 적용했다.
    -   ESLint는 코딩 스타일도 설정해줄 수 있지만 해당 프로젝트에서는 적용하지 않았다.
        \*\*\* eslint-plugin-prettier 뿐만 아니라 eslint-config-prettier을 함께 설치하면 prettier에 대한 포맷 오류만 뜬다.
-   Prettier
    -   협업시 통일된 코딩 스타일을 적용할 수 있다.
-   Auto fix in VS Code
    -   코드를 저장할 때 오류가 있는 코드를 자동으로 고쳐준다.
-   Husky + Lint-staged
    -   pre-commit hook를 통해 eslint, prettier를 통과해야 commit이 가능하도록 만들어 협업할 때 코드의 일관성을 유지할 수 있다.
-   Storybook
    -   3개의 프로젝트를 관리할 수 있는 프로젝트 특성상 자주 사용하는 UI 관리가 필요하다.
    -   개발자-비개발자와의 의사소통을 위해 필요하다.

## 컴포넌트 구조

-   [Atom design](https://atomicdesign.bradfrost.com/chapter-2/)을 참고했으며, 하나의 컴포넌트 폴더(ex. Button) 안에는
    스타일(.scss), 컴포넌트(.tsx), 스토리 파일(.stories.tsx)로 구성되어 있다.

## 실행하기

```
$ yarn
$ yarn start
```
