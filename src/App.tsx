import React from 'react'
import { Button } from 'components/atoms/Button/Button'
import './App.scss'

const onLogin = (): void => {
    console.log('login')
}

const App = (): JSX.Element => {
    return (
        <div>
            <Button size="small" onClick={onLogin} label="Log in" primary />
            <Button size="small" onClick={onLogin} label="Log in2" />
        </div>
    )
}

export default App
