import React from 'react'
import './Button.scss'

export interface ButtonProps {
    primary?: boolean
    backgroundColor?: string
    size?: 'small' | 'medium' | 'large'
    label: string
    onClick?: () => void
}

const defaultProps: ButtonProps = {
    primary: false,
    size: 'medium',
    label: 'Test',
}

export const Button = (props: ButtonProps): JSX.Element => {
    const { primary, size, backgroundColor, label, onClick } = props
    const mode = primary ? 'storybook-button--primary' : 'storybook-button--secondary'

    return (
        <button
            className={['storybook-button', `storybook-button--${size}`, mode].join(' ')}
            style={{ backgroundColor }}
            onClick={onClick}
        >
            {label}
        </button>
    )
}

Button.defaultProps = defaultProps
